#!/bin/bash

# export PICO_SDK_PATH=~/Projects/raspberry/pico-sdk
#
mkdir -p build
cd build
#cmake ..
cmake -DCMAKE_BUILD_TYPE=Debug ..
make -j4 
cd ..
