#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "functions.h"

#define BUTTON_PIN 13
#define BUTTON_PIN_2 0
#define LED_PIN 14
#define LED_PIN_2 2
const uint LED_DEFAULT_PIN = PICO_DEFAULT_LED_PIN;
 

void startup();
void blinkBoardLED(uint duration);

int main() {
    stdio_init_all();
   
    gpio_init(LED_DEFAULT_PIN);
    gpio_set_dir(LED_DEFAULT_PIN, GPIO_OUT);
    
    gpio_init(BUTTON_PIN);
    gpio_set_dir(BUTTON_PIN, GPIO_IN);
    gpio_pull_up(BUTTON_PIN);

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    gpio_init(BUTTON_PIN_2);
    gpio_set_dir(BUTTON_PIN_2, GPIO_IN);
    gpio_pull_up(BUTTON_PIN_2);

    gpio_init(LED_PIN_2);
    gpio_set_dir(LED_PIN_2, GPIO_OUT);


    startup();
    printf("Button LED Example\n");

    while (1) {
        if (gpio_get(BUTTON_PIN) == 0) {
            gpio_put(LED_PIN, 1);
            printf("Button 1 is pushed! LED is on.\n");
        } else {
            gpio_put(LED_PIN, 0);
        }


        if (gpio_get(BUTTON_PIN_2) == 0) {
            gpio_put(LED_PIN_2, 1);
            printf("Button 2 is pushed! LED is on.\n");
        } else {
            gpio_put(LED_PIN_2, 0);
        }
    }

    return 0;
}



void blinkBoardLED(uint duration) {
    gpio_put(LED_DEFAULT_PIN, 1);
    sleep_ms(duration);
    gpio_put(LED_DEFAULT_PIN, 0);
}

void startup() {
    mfSum(12,13);

    for (uint i = 0; i < 2; i++) {
        blinkBoardLED(200);
        sleep_ms(200);
    }
}




